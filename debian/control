Source: xmlrpc-c
Priority: optional
Section: libs
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper (>= 9),
 g++ (>= 4:7),
 libcurl4-openssl-dev | libcurl3-openssl-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://xmlrpc-c.sourceforge.net
Vcs-Git: https://salsa.debian.org/debian/xmlrpc-c.git
Vcs-Browser: https://salsa.debian.org/debian/xmlrpc-c

Package: libxmlrpc-c++8-dev
Section: libdevel
Architecture: any
Depends:
 libc6-dev,
 libxmlrpc-c++8v5 (= ${binary:Version}),
 libxmlrpc-core-c3-dev (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 xmlrpc-api-utils,
Conflicts:
 libxmlrpc-c++4-dev,
Breaks:
 libxmlrpc-core-c3-dev (<< 1.33.14-5),
Replaces:
 libxmlrpc-core-c3-dev (<< 1.33.14-5),
Description: Lightweight RPC library based on XML and HTTP [C++ development libraries]
 XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
 It converts the procedure call into an XML document, sends it to a remote
 server using HTTP, and gets back the response as XML.
 .
 This library provides a modular implementation of XML-RPC for C++.
 .
 Install this package if you wish to develop your own programs using this
 library.

Package: libxmlrpc-c++8v5
Architecture: any
Depends:
 libxmlrpc-core-c3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 libxmlrpc-c++8,
Replaces:
 libxmlrpc-c++8,
Description: Lightweight RPC library based on XML and HTTP [C++ runtime libraries]
 XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
 It converts the procedure call into an XML document, sends it to a remote
 server using HTTP, and gets back the response as XML.
 .
 This library provides a modular implementation of XML-RPC for C++.

Package: libxmlrpc-core-c3-dev
Section: libdevel
Architecture: any
Depends:
 libc6-dev,
 libxmlrpc-core-c3 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 xmlrpc-api-utils,
Description: Lightweight RPC library based on XML and HTTP [C development libraries]
 XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
 It converts the procedure call into an XML document, sends it to a remote
 server using HTTP, and gets back the response as XML.
 .
 This library provides a modular implementation of XML-RPC for C.
 .
 Install this package if you wish to develop your own programs using this
 library.

Package: libxmlrpc-core-c3
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Lightweight RPC library based on XML and HTTP [C runtime libraries]
 XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
 It converts the procedure call into an XML document, sends it to a remote
 server using HTTP, and gets back the response as XML.
 .
 This library provides a modular implementation of XML-RPC for C.

Package: xmlrpc-api-utils
Conflicts:
 xml-rpc-api2cpp,
 xml-rpc-api2txt,
Provides:
 xml-rpc-api2cpp,
 xml-rpc-api2txt,
Replaces:
 xml-rpc-api2cpp,
 xml-rpc-api2txt,
Architecture: any
Section: devel
Depends:
 libc6-dev,
 libfrontier-rpc-perl,
 libxmlrpc-c++8v5 (= ${binary:Version}),
 libxmlrpc-core-c3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Generate C++ wrapper classes for XML-RPC servers
 XML-RPC is a quick-and-easy way to make procedure calls over the Internet.
 It converts the procedure call into an XML document, sends it to a remote
 server using HTTP, and gets back the response as XML.
 .
 This package contains two programs:
  * xml-rpc-api2cpp, a utility for generating C++ wrapper classes based
    on an XML-RPC API, obtained by interrogating an XML-RPC server.
  * xml-rpc-api2txt, a utility for printing out an XML-RPC API as a
    text file.  The API is obtained by interrogating an XML-RPC server.
